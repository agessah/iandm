<?php
	$data = json_decode(file_get_contents("https://swapi.co/api/people"));
	
	$display = '<table border="1" cellpadding="5" cellspacing="0">';
	foreach ($data->results as $result):
		$display .= '<tr>
						<td><a href="' . $result->url . '">' . $result->name . '</a></td>
						<td>' . $result->height . '</td>
						<td>' . $result->mass . '</td>
						<td>' . $result->hair_color . '</td>
						<td>' . $result->skin_color . '</td>
						<td>' . $result->eye_color . '</td>						
						<td>' . $result->gender . '</td>
					</tr>';
	endforeach;
	
	echo $display .= '</table>';
?>
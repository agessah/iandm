/*
 * I&M Assignment
 */
package assignment;

import java.util.Arrays;

/**
 *
 * @author Agesa Usugah
 */
public class Assignment 
{
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        int n=9;//it is the number to be checked if prime
        
        int intArr[] = {1,3,2,0}; // array values to be searched
        int intKey = 2; // value to be searched
        
        
        System.out.println("If Prime: "+Prime.prime(n)); 
        System.out.println("Search Results: "+search(intArr, intKey));
    } 
    
    public static int search(int[] array, int key)
    {
        Arrays.sort(array); 
        int index = Arrays.binarySearch(array, key);
        return (index < 0) ? -1 : index;
    }
}
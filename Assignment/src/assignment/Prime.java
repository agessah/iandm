/*
 * I&M Assignment
 */
package assignment;

import java.util.function.Function;

/**
 * @author Agesa Usugah
 */
public class Prime 
{
    private static final Function<Integer, Boolean> CACHED = Memoizer.memoize(Prime::uncached);
 
    public static Boolean prime(int n) 
    { 
        return CACHED.apply(n);
    }
    
    private static boolean uncached(int n) 
    {        
        boolean prime = true;
        
        if (n<2)
        {  
            prime = false;
        }
        else
        {  
           for (int i=2; i<=(n/2); i++)
           {
                if (n%i==0)
                {      
                    prime = false;          
                    break;      
                }      
           }
        } 
        
        return prime;  
    }
}